import 'package:ahc/done.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart' as svg;

class Repair extends StatefulWidget{
  RepairState createState()=>RepairState();
}


class RepairState extends State<Repair>{
  var SelectedValue;
  var RadioSelectedValue;
  @override
  Widget build(BuildContext context) {
return Scaffold(
  appBar: PreferredSize(
    preferredSize: Size.fromHeight(75.0),
    child: AppBar(
      centerTitle: true,
      title: Column(
        children: [
          SizedBox(height: 20,),
          Text("Repair Request")
        ],
      ),
    ),
  ),
  body: Padding(
    padding: EdgeInsets.only(left: 20,right: 20,top: 20,bottom: 20),
    child: Column(
      children: [

        Container(
          height: 46,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(7)),
            border: Border.all(color: Colors.black12)
          ),
          child: Row(
            children: [
              Expanded(child: Container(child: svg.SvgPicture.asset("assets/menu/rmenu.svg"),),flex: 1,),
              Expanded(child: Container(child:TextFormField(
                style: TextStyle(fontSize: 16),
                decoration: InputDecoration(
                  border: InputBorder.none,
                ),
              ),),flex: 5,)
            ],
          ),
        ),
        SizedBox(height: 20,),
        Row(
          children: [
            Container(
              margin: EdgeInsets.only(left: 30),
              width: 80,
              height: 25,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(20)),
                color: Colors.blue
              ),
              child: Center(child: Text("Check",style: TextStyle(color: Colors.white),)),
            ),
            SizedBox(width: 20,),
            Text("Warenty",style: TextStyle(color: Colors.green),)
          ],
        ),
        SizedBox(height: 20,),
        Container(
          height: 46,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(7)),
              border: Border.all(color: Colors.black12)
          ),
          child: Row(
            children: [
              Expanded(child: Container(child: svg.SvgPicture.asset("assets/menu/battery.svg"),),flex: 1,),
              Expanded(child: Text("Battery Problem ?",style: TextStyle(fontSize: 10),),flex: 1,),
              Expanded(child: Container(
                child: Row(
                  children: [
                    Expanded(
                      child: Row(
                        children: [
                          Radio(
                            onChanged: (v){
                              setState(() {
                                RadioSelectedValue=v;

                              });
                            },
                            value: 1,
                            groupValue: RadioSelectedValue,

                          ),
                          Text("Yes")
                        ],
                      ),
                    ),
                    Expanded(
                      child: Row(
                        children: [
                          Radio(
                            onChanged: (v){
                              setState(() {
                                RadioSelectedValue=v;

                              });
                            },
                            value: 2,
                            groupValue: RadioSelectedValue,

                          ),
                          Text("No")
                        ],
                      ),

                    ),
                  ],
                ),
              ),flex: 2,)
            ],
          ),
        ),
        SizedBox(height: 20,),
        Container(
          height: 46,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(7)),
              border: Border.all(color: Colors.black12)
          ),
          child: Row(
            children: [
              Expanded(child: Container(child: svg.SvgPicture.asset("assets/menu/alert.svg"),),flex: 1,),
              Expanded(child: Container(child:DropdownButtonFormField(
                icon: Icon(Icons.keyboard_arrow_down_outlined,color: Colors.blue,size: 30,),
                decoration: InputDecoration.collapsed(hintText: "Hearing Aid Compaints"),
                onChanged: (v){
                  setState(() {
                    SelectedValue=v;

                  });
                },
                value: SelectedValue,
                items: ["One","Two","Three"].map((v){
                  return DropdownMenuItem(child: Text(v),value: v,);
                }).toList(),
              ),),flex: 5,)
            ],
          ),
        ),
        SizedBox(height: 20,),
        Container(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: (){
Navigator.of(context).push(MaterialPageRoute(builder: (context)=>Done("Repaire Request Confirmed !")));
                },
                child: Container(
                  margin: EdgeInsets.only(left: 10,right: 5,),
                  height: 44,
                  width: 120,
                  decoration: BoxDecoration(color: Colors.blue,borderRadius: BorderRadius.all(Radius.circular(8))),
                  child: Center(child: Text("Submit",style: TextStyle(color: Colors.white),)),
                ),
              ),
              GestureDetector(
                onTap: (){
Navigator.of(context).pop();
                },
                child: Container(
                  margin: EdgeInsets.only(left: 10,right: 5,),
                  height: 44,
                  width: 120,
                  decoration: BoxDecoration(color: Colors.blue,borderRadius: BorderRadius.all(Radius.circular(8))),
                  child: Center(child: Text("Cancel",style: TextStyle(color: Colors.white),)),
                ),
              ),
            ],
          ),
        ),

      ],
    ),
  ),
);
  }

}