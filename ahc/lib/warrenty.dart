import 'package:ahc/repair.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart' as svg;
import 'package:toast/toast.dart';
class WarrentyList extends StatefulWidget{
  WarrentyListState createState()=>WarrentyListState();
}
class WarrentyListState extends State<WarrentyList>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(75.0),
        child: AppBar(
          centerTitle: true,
          title: Column(
            children: [
              SizedBox(height: 20,),
              Text("Warranty and Repair")
            ],
          ),
        ),
      ),
      body: ListView.separated(
        itemCount: 4,
        separatorBuilder: (_,__)=>Divider(height: 1,color: Colors.black12,),
        itemBuilder: (BuildContext context,int pos){
          return Container(
            padding: EdgeInsets.all(10),
            height: 80,
            child: Row(
              children: [
Expanded(child: Column(
  crossAxisAlignment: CrossAxisAlignment.start,
  mainAxisAlignment: MainAxisAlignment.center,
  children: [
  Text("Sr 12345678 (L)",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
  SizedBox(height: 5,),
  Text("09/4/2021 "+" 09/4/2021",style: TextStyle(fontSize: 10,color: Colors.black54),),

],),flex: 3,),


       Expanded(child: GestureDetector(
         onTap: (){
           Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context)=>Repair()));

         },
         child: Container(
           child: Row(children: [

                        Padding(padding: EdgeInsets.only(right: 5),child: svg.SvgPicture.asset("assets/menu/tool.svg",height: 15,)),
                        Text("Repair",style: TextStyle(color: Colors.blue,fontSize: 15),)

                      ],),
         ),
       ),flex: 1,),

              ],
            ),
          );
        },
      ),
    );
  }

}