import 'dart:convert' as json;

import 'package:ahc/dashboard.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:toast/toast.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart' as SH;
class verificationotp extends StatefulWidget{
  var number;
  verificationotp(this.number);

  VerificationOtpState createState()=>VerificationOtpState(this.number);

}

class VerificationOtpState extends State<verificationotp>{


  var number;
  VerificationOtpState(this.number);

  var Otp=TextEditingController();

  GlobalKey<ScaffoldState> key=GlobalKey<ScaffoldState>();


  CheckOtp()async{

    SH.SharedPreferences sharepreference=await SH.SharedPreferences.getInstance();


    var request=http.MultipartRequest('POST',Uri.parse("http://164.52.205.22/api/validateOtp.php"));
    request.fields.addAll({
      'mobile':"9841095322",
      'otp':Otp.text.trim().toString(),
    });

    http.StreamedResponse response=await request.send();


    if(response.statusCode==200){
      var Result=json.jsonDecode(await response.stream.bytesToString());
      if(Result["error"]==false){
        sharepreference.setString("session_id", Result["session_id"]);
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
            dashboard()), (Route<dynamic> route) => false);
      }else{
        Toast.show("Wrong OTP", context);
      }

    }



  }

GODashBoard()async{

  SH.SharedPreferences sharepreference=await SH.SharedPreferences.getInstance();
  sharepreference.getString("session_id");
  Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
      dashboard()), (Route<dynamic> route) => false);

}


  @override
  Widget build(BuildContext context) {


    return Scaffold(
      key: key,
      body: ListView(
        children: [
          Container(
            margin: EdgeInsets.all(20),
            height: 70,
            child: Image.asset("assets/logo.png"),
          ),
          SizedBox(height: 100,),
          Container(
            height: 93,
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(left: 60,right: 60),
            child: Column(
              children: [
                Text("Verification",style: TextStyle(color: Colors.blue,fontWeight: FontWeight.bold,fontSize: 26),),
                SizedBox(height: 10,),
                Text("Insert your code here:",style: TextStyle(fontSize: 15),textAlign: TextAlign.center,)
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 30,right: 30,bottom: 20),
            height: 50,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(5)),
              border: Border.all(color: Colors.black12)
            ),
            child: TextFormField(
              keyboardType: TextInputType.number,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 25,letterSpacing: 5),
              decoration: InputDecoration(
             focusedBorder: OutlineInputBorder(
               borderSide: BorderSide.none
             ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide.none
                ),
                border: OutlineInputBorder(
                  borderSide: BorderSide.none
                )
              ),
              controller: Otp,
              enabled: true,
            ),
          ),
          GestureDetector(
            onTap: (){
             //  CheckOtp();
           GODashBoard();

            },
            child: Container(
              height: 50,
              decoration: BoxDecoration(
                color: Colors.blue,
                borderRadius: BorderRadius.all(Radius.circular(4)),
              ),
              child: Center(child: Text("Continue",style: TextStyle(color: Colors.white,fontSize: 18),)),
              margin: EdgeInsets.only(left: 30,right: 30),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 25,right: 25,top: 10),
            child: Row(
              children: [
                Expanded(child: GestureDetector(onTap:(){print("OK!");},child: Container(padding:EdgeInsets.only(top: 5,bottom: 5),child: Text("Resend Code 30sec",style: TextStyle(color: Colors.blue,fontSize: 15),),))),
                Expanded(child: GestureDetector(onTap:(){
                  Navigator.pop(context);

                },child: Container(padding:EdgeInsets.only(top: 5,bottom: 5),child: Text("Change Number",style: TextStyle(color: Colors.blue,fontSize: 15),textAlign: TextAlign.end,),))),

              ],
            ),
          ),
        ],
      ),
    );
  }

}