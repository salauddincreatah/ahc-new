import 'dart:convert' as Json;

import 'package:ahc/dashboard.dart';
import 'package:flutter/services.dart';

import 'package:ahc/verificationotp.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:http/http.dart' as http;

class login extends StatefulWidget{
  loginState createState()=>loginState();

}
class loginState extends State<login>{




  var DefaultNumberCode="+60";

  List NumberCodes=["+60","+80","+91"];


  var number_controller=TextEditingController();

  
  
  sendOtp()async{
    
    var request=http.MultipartRequest('POST',Uri.parse("http://164.52.205.22/api/getOtp.php"));
    request.fields.addAll({
      'mobile_no':'9841095322'
    });
    http.StreamedResponse response=await request.send();
    
    if(response.statusCode==200){
 var Recived=Json.jsonDecode(await response.stream.bytesToString());
        print("OK "+Recived.toString());
        Navigator.push(context, MaterialPageRoute(builder: (context)=>verificationotp(number_controller.text.toString())));


    }else {
     print("Faild");
    }
    
    
  }
  
  
  

  @override
  Widget build(BuildContext context) {



    showNumberCode(){
      return NumberCodes.map((v){
        return DropdownMenuItem(
          child: Text(v),
          value: v,
        );

      }).toList();


    }


   return SafeArea(child: Scaffold(
     body: ListView(
       children: [
         Container(
           margin: EdgeInsets.all(20),
           height: 70,
           child: Image.asset("assets/logo.png"),
         ),
         SizedBox(height: 100,),
         Container(
           height: 100,
           width: MediaQuery.of(context).size.width,
           margin: EdgeInsets.only(left: 60,right: 60),
           child: Column(
             children: [
                  Text("Mobile Number",style: TextStyle(color: Colors.blue,fontWeight: FontWeight.bold,fontSize: 26),),
                  SizedBox(height: 10,),
                  Text("The code will be sent to the registered mobile number ",style: TextStyle(fontSize: 15),textAlign: TextAlign.center,)
             ],
           ),
         ),

         Container(
           height: 50,
           margin: EdgeInsets.only(left: 30,right: 30),
           decoration: BoxDecoration(
             borderRadius: BorderRadius.all(Radius.circular(4)),
             border: Border.all(color: Colors.black12)
           ),
           child: Row(
             children: [
               Expanded(child: Container(
                 padding: EdgeInsets.only(left: 6),
                 child: DropdownButtonFormField(
                   value: DefaultNumberCode,
                   decoration: InputDecoration.collapsed(),
                   onChanged: (v){
                     setState(() {
                       DefaultNumberCode=v;
                     });
                   },

                   items: showNumberCode(),

                 ),
               ),flex: 1,),
               Expanded(child: Container(
                 padding: EdgeInsets.only(left: 5),
                 child: TextField(
                   keyboardType: TextInputType.number,
                   controller: number_controller,
                   autofocus: false,
                 decoration: InputDecoration(
                   hintText: "Mobile Number",
                   focusedBorder: OutlineInputBorder(
                     borderSide: BorderSide.none
                   ),
                   border: OutlineInputBorder(
                     borderSide: BorderSide.none
                   )
                 ),

               ),),flex: 4,),
             ],
           ),
         ),
      SizedBox(height: 20,),
         GestureDetector(
           onTap: (){
             if(number_controller.text.toString().isEmpty){
               Toast.show("Please Enter Number", context);
             }else{
               sendOtp();
             }

           },
           child: Container(
             height: 50,
             decoration: BoxDecoration(
               color: Colors.blue,
                 borderRadius: BorderRadius.all(Radius.circular(4)),
             ),
             child: Center(child: Text("Continue",style: TextStyle(color: Colors.white,fontSize: 18),)),
             margin: EdgeInsets.only(left: 30,right: 30),
           ),
         )
       ],
     ),

   ));
  }

}