import 'dart:convert';

import 'package:ahc/appointment.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart' as svg;
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart' as toast;
import 'package:shared_preferences/shared_preferences.dart' as SH;

class MyAppointment extends StatefulWidget{

  MyAppointmentState createState()=>MyAppointmentState();

}
class MyAppointmentState extends State<MyAppointment> {


  List Data;
  //
  // CancelAppointment(pos)async{
  //
  //   SH.SharedPreferences sharedPreferences=await SH.SharedPreferences.getInstance();
  //   var SessionID=sharedPreferences.getString("session_id");
  //
  //   var request=http.MultipartRequest("POST",Uri.parse("http://164.52.205.22/api/cancelAppointment.php"));
  //
  //   request.fields.addAll({
  //
  //     "session_id":SessionID,
  //     "appointment_id":pos
  //   });
  //
  //   http.StreamedResponse response=await request.send();
  //
  //   if(response.statusCode==200){
  //     var result=await response.stream.bytesToString();
  //     var resultJSON=json.decode(result);
  //     if(resultJSON["error"]==false){
  //       toast.Toast.show("Cancel Successfull", context);
  //     }
  //
  //   }
  // }
  //
  //
  //
  // Future showList()async{
  //
  //   var request=http.MultipartRequest("POST",Uri.parse("http://164.52.205.22/api/getAppointment.php"));
  //
  //   request.fields.addAll({
  //     'session_id':'ASTR-NAGP-000-7955'
  //   });
  //   http.StreamedResponse response=await request.send();
  //
  //   if(response.statusCode==200){
  //     var Result=await response.stream.bytesToString();
  //     var result_json=json.decode(Result);
  //
  //     setState(() {
  //       Data=result_json;
  //     });
  //     print(result_json);
  //
  //   }
  //
  // }
  
  
@override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(75.0),
        child: AppBar(
          centerTitle: true,
          title:Padding(padding: EdgeInsets.only(top: 20),child:  Text("My Appointments"),),
        ),
      ),
      body: Container(
        margin: EdgeInsets.all(20),
        child: Column(
          children: [
            Expanded(
              child: ListView.builder(
                  itemCount: 3,
                  itemBuilder: (BuildContext context,int pos){

                    return Column(
                      children: [
                        Container(
                          padding: EdgeInsets.only(left: 8,right: 8,top: 5,bottom: 5),
                          height: 70,
                          child:Column(
                            children: [
                              Row(
                                children: [
                                  Text("4/3/2001".toString(),style: TextStyle(color: Colors.black45,fontSize: 10),),
                                  SizedBox(width: 14,),
                                  Text("6:12 AM"+" - "+"7:00 AM",style: TextStyle(color: Colors.black45,fontSize: 10),),

                                ],
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 10),
                                child: Row(
                                  children: [
                                    Expanded(child: Text("ENT- DR MEGHNA".toString(),style: TextStyle(fontSize:16,color: Colors.black,fontWeight: FontWeight.bold),),flex: 2,),
                                    Expanded(child:
                                    Row(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                      Container(
                                        child:Row(
                                          children: [
                                            GestureDetector(
                                              onTap: (){
                                                print("Edit"+pos.toString());
                                              },
                                              child: Padding(
                                                padding:EdgeInsets.only(right: 5),
                                                child: Row(
                                                  children: [
                                                    svg.SvgPicture.asset("assets/menu/edit.svg"),
                                                    Padding(padding: EdgeInsets.only(left: 5,right: 5),child: Text("Edit",style: TextStyle(fontSize: 15),))
                                                  ],
                                                ),
                                              ),
                                            ),
                                            GestureDetector(
                                              onTap: (){

                                              },
                                              child: Row(
                                                children: [
                                                  svg.SvgPicture.asset("assets/menu/delete.svg"),
                                                  Padding(padding: EdgeInsets.only(left: 5,right: 5),child: Text("Delete",style: TextStyle(fontSize: 15),))
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      )  
                                    ],),flex: 2,),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                        Divider(thickness: 2.0,)

                      ],
                    );

                  }
                  ),
            ),
            GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>Appointment()));
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 50,
                decoration: BoxDecoration(
                  color: Colors.blue,
                borderRadius: BorderRadius.all(Radius.circular(6))
                ),
                
                child: Center(child: Text("Book an appointment",style: TextStyle(color: Colors.white,fontSize: 18),)),
              ),
            )
          ],
        ),
      ),
    );
  }

}