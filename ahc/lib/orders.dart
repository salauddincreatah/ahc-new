import 'package:ahc/payment.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart' as svg;

class Orders extends StatefulWidget{
  OrdersState createState()=>OrdersState();
}
class OrdersState extends State<Orders>{
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(75.0),
        child: AppBar(
          centerTitle: true,
          title:Column(
            children: [
              SizedBox(height: 15,),
              Text("Orders")
            ],
          ),
          backgroundColor: Colors.blue,
        ),
      ),
      body: Column(
        children: [
          Container(
            margin: EdgeInsets.only(left: 20,right: 10),
            padding: EdgeInsets.only(top: 10),
            height: 100,
            child: Column(
              children: [
                Row(children: [
                  Expanded(child: Text("03/3/2021",style: TextStyle(color: Colors.black45,fontSize: 12),),flex: 1,),
                  Expanded(
                    flex: 2,
                    child: Container(
                      child:Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text("Delivery on 11/4/2021",style: TextStyle(color: Colors.black45,fontSize: 12),textAlign: TextAlign.end,),
                          svg.SvgPicture.asset("assets/menu/delivery.svg",height: 15,),

                        ],
                      ),
                    ),
                  ),




                ],),
                SizedBox(height: 10,),
                Row(
                  children: [

                    Expanded(child: Text("Start Key BTE Aries 675 (L) ",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 12),),flex: 1,),
                    Expanded(child: Text("\$13,300 ",style: TextStyle(color: Colors.blue,fontWeight: FontWeight.bold,fontSize: 12),textAlign: TextAlign.end,),flex: 1,)


                  ],
                ),
                SizedBox(height: 7,),
                Row(
                  children: [
                    Expanded(child: Row(
                      children: [
                        Text("Balance ",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold,color: Colors.black),),
                        Text("\t \$4,000 ",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold,color: Colors.blue),)


                      ],
                    ),flex: 5,),
                    Expanded(child: GestureDetector(
                        onTap: (){
                          Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context)=>Payment()));
                        },
                        child: Container(height: 22,child: Center(child: Text("Pay now",style: TextStyle(color: Colors.white),textAlign: TextAlign.end,)),decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(10.0)),color: Colors.blue,),)),flex: 1,)


                  ],
                ),
                  Divider()
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20,right: 10),
            padding: EdgeInsets.only(top: 10),
            height: 100,
            child: Column(
              children: [
                Row(children: [
                  Expanded(child: Text("03/3/2021",style: TextStyle(color: Colors.black45,fontSize: 12),),flex: 1,),
                  Expanded(
                    flex: 2,
                    child: Container(
                      child:Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text("Ready For Fiting",style: TextStyle(color: Colors.black45,fontSize: 12),textAlign: TextAlign.end,),
                          svg.SvgPicture.asset("assets/menu/tool.svg",height: 15,),

                        ],
                      ),
                    ),
                  ),




                ],),
                SizedBox(height: 10,),
                Row(
                  children: [

                    Expanded(child: Text("Start Key BTE Aries 675 (L) ",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 12),),flex: 1,),
                    Expanded(child: Text("\$13,300 ",style: TextStyle(color: Colors.blue,fontWeight: FontWeight.bold,fontSize: 12),textAlign: TextAlign.end,),flex: 1,)


                  ],
                ),
                SizedBox(height: 7,),
                Row(
                  children: [
                    Expanded(child: Row(
                      children: [
                        Text("Balance ",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold,color: Colors.black),),
                        Text("\t \$4,000 ",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold,color: Colors.blue),)


                      ],
                    ),flex: 5,),
                    // Expanded(child: Container(height: 22,child: Center(child: Text("Pay now",style: TextStyle(color: Colors.white),textAlign: TextAlign.end,)),decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(10.0)),color: Colors.blue,),),flex: 1,)


                  ],
                ),

                Divider(),
              ],
            ),
          )
        ],
      ),
      // body:ListView.builder(itemBuilder: (BuildContext context,count){
      //
      //   return Container(
      //     margin: EdgeInsets.only(left: 20,right: 10),
      //     padding: EdgeInsets.only(top: 10),
      //     height: 100,
      //     child: Column(
      //       children: [
      //         Row(children: [
      //           Expanded(child: Text("03/3/2021",style: TextStyle(color: Colors.black45),),flex: 1,),
      //           Expanded(
      //             flex: 2,
      //             child: Container(
      //               child:Row(
      //                 crossAxisAlignment: CrossAxisAlignment.end,
      //                 mainAxisAlignment: MainAxisAlignment.end,
      //                 children: [
      //                  Text("Delivery on 11/4/2021",style: TextStyle(color: Colors.black45,),textAlign: TextAlign.end,),
      //                   svg.SvgPicture.asset("assets/menu/delivery.svg",height: 15,),
      //
      //                 ],
      //               ),
      //             ),
      //           ),
      //
      //
      //
      //
      //         ],),
      //         SizedBox(height: 10,),
      //         Row(
      //           children: [
      //
      //             Expanded(child: Text("Start Key BTE Aries 675 (L) ",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 17),),flex: 1,),
      //             Expanded(child: Text("\$13,300 ",style: TextStyle(color: Colors.blue,fontWeight: FontWeight.bold,fontSize: 16),textAlign: TextAlign.end,),flex: 1,)
      //
      //
      //           ],
      //         ),
      //         SizedBox(height: 7,),
      //         Row(
      //           children: [
      //             Expanded(child: Row(
      //               children: [
      //                 Text("Balance ",style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold,color: Colors.black),),
      //                 Text("\t \$4,000 ",style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold,color: Colors.blue),)
      //
      //
      //               ],
      //             ),flex: 5,),
      //             Expanded(child: Container(height: 22,child: Center(child: Text("Pay now",style: TextStyle(color: Colors.white),textAlign: TextAlign.end,)),decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(10.0)),color: Colors.blue,),),flex: 1,)
      //
      //
      //           ],
      //         )
      //
      //
      //       ],
      //     ),
      //   );
      // })
    );
  }

}