import 'package:ahc/login.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart' as SH;

import 'dashboard.dart';
void main() {
  runApp(MaterialApp(title: "Aural Hearing Care",home:Splash(),debugShowCheckedModeBanner: false,));
}

class Splash extends StatefulWidget{
  SplashState createState()=> SplashState();
}
class SplashState extends State<Splash>{
  chekislogin()async{
    SH.SharedPreferences sharepreference=await SH.SharedPreferences.getInstance();

    var SessionId=sharepreference.getString("session_id");
    if(SessionId!=null){
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>dashboard()));
    }else{
      Future.delayed(Duration(seconds: 3),(){
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>login()));
      });
    }

  }


  @override
  void initState() {
    chekislogin();
    super.initState();


  }


  @override
  Widget build(BuildContext context) {

    return SafeArea(
      child: Scaffold(
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Center(
            child: Container(
              margin: EdgeInsets.all(30),
              height: 40,
              width: MediaQuery.of(context).size.width,
              child: Image.asset("assets/logo.png"),
            ),
          ),
        ),
      ),
    );
  }

}