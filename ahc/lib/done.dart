import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart' as svg;

class Done extends StatefulWidget{
  var Name;
  Done(this.Name);
  DoneState createState()=>DoneState(this.Name);
}
class DoneState extends State<Done>{
  var Name;
  DoneState(this.Name);
  @override
  Widget build(BuildContext context) {
  return SafeArea(
    child: Scaffold(
      body: Stack(
        children: [
          Row(
            children: [
           Expanded(flex: 1,child: GestureDetector(
               onTap: (){
                 Navigator.of(context).pop();
               },
               child: Container(height: 50, child:  Icon(Icons.arrow_back_ios,color: Colors.blue,size: 20,),))),
              Expanded(child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                svg.SvgPicture.asset("assets/menu/success.svg"),
                Padding(padding:EdgeInsets.only(left: 10),child: Text(Name.toString(),style: TextStyle(color: Colors.black,fontSize: 20),))
              ],),flex: 12,)



            ],
          ),
          Positioned(
              child: Container(
                child: Center(child: svg.SvgPicture.asset("assets/menu/circlehand.svg")),)),

          Positioned(
            bottom: 20,
            left: 0,
            right: 0,
            child: GestureDetector(
              onTap: (){
                Navigator.of(context).pop();
              },
              child: Container(
                height: 40,
                margin: EdgeInsets.only(left: 140,right: 140),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(6)),
                  color: Colors.blue

                ),
                child: Center(child: Text("Done",style: TextStyle(color: Colors.white,),)),
              ),
            ),
          )
        ],
      ),
    ),
  );
  }

}