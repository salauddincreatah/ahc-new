import 'package:ahc/appointment.dart';
import 'package:ahc/login.dart';
import 'package:ahc/myappoinments.dart';
import 'package:ahc/orders.dart';
import 'package:ahc/repair.dart';
import 'package:ahc/sparerequest.dart';
import 'package:ahc/warrenty.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart' as svg;
import 'package:shared_preferences/shared_preferences.dart' as SH;
class dashboard extends StatefulWidget{

  dashboardState createState()=>dashboardState();

}
class dashboardState extends State<dashboard>{




  _logout()async{
    SH.SharedPreferences sharedPreferences=await SH.SharedPreferences.getInstance();
    sharedPreferences.setString("session_id",null);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>login()));
  }


  @override
  Widget build(BuildContext context) {
   return SafeArea(
     child: Scaffold(
       appBar: PreferredSize(
         preferredSize: Size.fromHeight(75.0),
         child: AppBar(
           title: Column(
             crossAxisAlignment: CrossAxisAlignment.start,
             children: [
               SizedBox(height: 9,),
               Text("Welcome",style: TextStyle(fontSize: 25),),
               SizedBox(height: 2,),
               Text("Parbua",style: TextStyle(fontSize: 14),)
             ],
           ),

         ),
       ),
       drawer: Drawer(
         child: ListView(
           children: [Column(
             children: [

               Stack(
                 children: [
                   Container(
                     height: 200,
                     color: Colors.black12,
                   ),

                   Positioned(
                     left: 17,
                     right: 0,
                     top: 0,
                     bottom: 0,
                     child: Row(
                       crossAxisAlignment: CrossAxisAlignment.center,
                       mainAxisAlignment: MainAxisAlignment.start,
                       children: [
                         Container(
                           height: 70,
                           width: 70,
                           decoration: BoxDecoration(
                               shape: BoxShape.circle,
                               image: DecorationImage(
                                   image: NetworkImage("https://www.shareicon.net/data/512x512/2016/09/01/822711_user_512x512.png"),
                                   fit: BoxFit.cover
                               )
                           ),
                         ),
                         Container(
                           padding: EdgeInsets.only(left: 7),
                           child: Column(
                             crossAxisAlignment: CrossAxisAlignment.start,
                             mainAxisAlignment: MainAxisAlignment.center,
                             children: [
                               Text("Prabha Vasan",style: TextStyle(color: Colors.blue,fontSize: 20,fontWeight: FontWeight.bold),),
                               SizedBox(height: 3,),
                               Text("Chenni",style: TextStyle(color: Colors.blue,fontSize: 14),)

                             ],),
                         )
                       ],
                     ),
                   )

                 ],
               ),
               Container(
                 margin: EdgeInsets.only(left: 20,right: 10,top: 30),
                 height: MediaQuery.of(context).size.height/1.6,
                 child: Stack(
                   children: [
                     Column(
                       children: [

                         GestureDetector(
                           onTap: (){
                             Navigator.push(context, MaterialPageRoute(builder: (context)=>MyAppointment()));
                           },
                           child: Container(
                             padding: EdgeInsets.all(7),
                             child: Row(
                               children: [
                                 svg.SvgPicture.asset("assets/menu/appointment.svg",height: 23,),
                                 Padding(padding:EdgeInsets.only(left: 7),child: Text("My Appoinments",style: TextStyle(color: Colors.black54,fontSize: 18),))
                               ],
                             ),
                           ),
                         ),
                         GestureDetector(
                           onTap: (){
                             Navigator.of(context).push(MaterialPageRoute(builder: (context)=>Orders()));
                           },
                           child: Container(
                             padding: EdgeInsets.all(7),
                             child: Row(
                               children: [
                                 svg.SvgPicture.asset("assets/menu/orders.svg",height: 23,),
                                 Padding(padding:EdgeInsets.only(left: 7),child: Text("Orders",style: TextStyle(color: Colors.black54,fontSize: 18),))
                               ],
                             ),
                           ),
                         ),
                         GestureDetector(
                           onTap: (){
                             Navigator.of(context).push(MaterialPageRoute(builder: (context)=>WarrentyList()));
                           },
                           child: Container(
                             padding: EdgeInsets.all(7),
                             child: Row(
                               children: [
                                 svg.SvgPicture.asset("assets/menu/warrantyandrepair.svg",height: 23,),
                                 Padding(padding:EdgeInsets.only(left: 7),child: Text("Warranty & Repairs",style: TextStyle(color: Colors.black54,fontSize: 18),))
                               ],
                             ),
                           ),
                         ),
                         GestureDetector(
                           onTap: (){
                             Navigator.of(context).push(MaterialPageRoute(builder: (context)=>SpareRequest()));
                           },
                           child: Container(
                             padding: EdgeInsets.all(7),
                             child: Row(
                               children: [
                                 svg.SvgPicture.asset("assets/menu/spare.svg",height: 23,),
                                 Padding(padding:EdgeInsets.only(left: 7),child: Text("Spare Request",style: TextStyle(color: Colors.black54,fontSize: 18),))
                               ],
                             ),
                           ),
                         ),
                         GestureDetector(
                           onTap: (){
                             print("Pressed Me");
                           },
                           child: Container(
                             padding: EdgeInsets.all(7),
                             child: Row(
                               children: [
                                 svg.SvgPicture.asset("assets/menu/account.svg",height: 23,),
                                 Padding(padding:EdgeInsets.only(left: 7),child: Text("Account Settings",style: TextStyle(color: Colors.black54,fontSize: 18),))
                               ],
                             ),
                           ),
                         ),
                         GestureDetector(
                           onTap: (){
                             print("Pressed Me");
                           },
                           child: Container(
                             padding: EdgeInsets.all(7),
                             child: Row(
                               children: [
                                 svg.SvgPicture.asset("assets/menu/help.svg",height: 23,),
                                 Padding(padding:EdgeInsets.only(left: 7),child: Text("Help",style: TextStyle(color: Colors.black54,fontSize: 18),))
                               ],
                             ),
                           ),
                         ),



                       ],
                     ),

                     Positioned(
                         bottom: 40,
                         child: GestureDetector(
                           onTap: (){
                             _logout();
                           },
                           child: Container(
                             padding: EdgeInsets.only(left: 10,),
                             width: MediaQuery.of(context).size.width,
                             height: 40,
                             child: Row(
                               children: [
                                 Icon(Icons.login,color: Colors.blue,),
                                 Padding(padding:EdgeInsets.only(left: 7),child: Text("Logout",style: TextStyle(fontSize: 18),))
                               ],
                             ),),
                         ))

                   ],

                 ),
               ),


             ],
           )],
         ),
       ),
       body: ListView(
         children: [
           Padding(
             padding: EdgeInsets.all(20),
             child: Column(
               children: [
                 Container(
                   margin: EdgeInsets.only(top: 10),
                   height: 65,
                   child: Center(child: Image.asset("assets/logo.png")),
                 ),
                 Container(
                   child: SingleChildScrollView(
                     scrollDirection: Axis.horizontal,
                     child: Row(
                       children: [
                         Container(
                           height: 200,
                           child: Card(
                             child: Image.asset("assets/banner.jpg"),
                           ),
                         ),
                         Container(
                           height: 200,
                           child: Card(
                             child: Image.asset("assets/banner.jpg"),
                           ),
                         ),
                         Container(
                           height: 200,
                           child: Card(
                             child: Image.asset("assets/banner.jpg"),
                           ),
                         )
                       ],
                     ),
                   ),
                 ),
                 SizedBox(height: 10,),
                 Container(
                   child: Row(
                     crossAxisAlignment: CrossAxisAlignment.center,
                     mainAxisAlignment: MainAxisAlignment.center,
                     children: [
                       svg.SvgPicture.asset("assets/menu/dot.svg",height: 7,),
                       SizedBox(width: 3,),
                       svg.SvgPicture.asset("assets/menu/dot.svg",height: 7,),
                       SizedBox(width: 3,),
                       svg.SvgPicture.asset("assets/menu/dot.svg",height: 7,),
                     ],
                   ),
                 ),
                 SizedBox(height: 10,),
                 Row(
                   children: [

                     Expanded(
                       flex: 1,
                       child: GestureDetector(
                         onTap: (){
                           Navigator.push(context, MaterialPageRoute(builder: (context)=>MyAppointment()));
                         },
                         child: Container(
                           height: 130,
                           decoration: BoxDecoration(
                               borderRadius: BorderRadius.all(Radius.circular(50))
                           ),
                           child: Card(
                             color: Colors.blue,
                             shape: RoundedRectangleBorder(
                                 borderRadius: BorderRadius.all(Radius.circular(10))
                             ),
                             child: Column(
                               crossAxisAlignment: CrossAxisAlignment.start,
                               children: [
                                 Padding(padding:EdgeInsets.only(left: 20,top: 30),child: svg.SvgPicture.asset("assets/menu/date.svg",color: Colors.white,height: 35,)),
                                 Padding(padding:EdgeInsets.only(left: 20,top: 10),child: Text("My Appointments",style: TextStyle(color: Colors.white,fontSize: 15,fontWeight: FontWeight.bold),))
                               ],
                             ),
                           ),

                         ),
                       ),
                     ),

                     Expanded(
                       flex: 1,
                       child: GestureDetector(
                         onTap: (){
                           Navigator.push(context, MaterialPageRoute(builder: (context)=>Orders()));

                         },
                         child: Container(
                           height: 130,
                           child: Card(
                             shape: RoundedRectangleBorder(
                                 borderRadius: BorderRadius.all(Radius.circular(10))
                             ),
                             child: Column(
                               crossAxisAlignment: CrossAxisAlignment.start,
                               children: [
                                 Padding(padding:EdgeInsets.only(left: 20,top: 30),child: svg.SvgPicture.asset("assets/menu/orders.svg",height: 35,)),
                                 Padding(padding:EdgeInsets.only(left: 20,top: 10),child: Text("Orders",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),))
                               ],
                             ),
                           ),

                         ),
                       ),
                     ),


                   ],
                 ),
                 Row(
                   children: [
                     Expanded(
                       flex: 1,
                       child: GestureDetector(
                         onTap: (){
                           Navigator.push(context, MaterialPageRoute(builder: (context)=>WarrentyList()));

                         },
                         child: Container(
                           height: 130,
                           child: Card(
                             shape: RoundedRectangleBorder(
                                 borderRadius: BorderRadius.all(Radius.circular(10))
                             ),
                             child: Column(
                               crossAxisAlignment: CrossAxisAlignment.start,
                               children: [
                                 Padding(padding:EdgeInsets.only(left: 20,top: 30),child: svg.SvgPicture.asset("assets/menu/warrentyhome.svg",height: 35,)),
                                 Padding(padding:EdgeInsets.only(left: 20,top: 10),child: Text("Warrenty & Repair",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),))
                               ],
                             ),
                           ),

                         ),
                       ),
                     ),
                     Expanded(
                       flex: 1,
                       child: GestureDetector(
                         onTap: (){
                           Navigator.push(context, MaterialPageRoute(builder: (context)=>SpareRequest()));

                         },
                         child: Container(
                           height: 130,
                           child: Card(
                             shape: RoundedRectangleBorder(
                                 borderRadius: BorderRadius.all(Radius.circular(10))
                             ),
                             child: Column(
                               crossAxisAlignment: CrossAxisAlignment.start,
                               children: [
                                 Padding(padding:EdgeInsets.only(left: 20,top: 30),child: svg.SvgPicture.asset("assets/menu/spare.svg",height: 35,)),
                                 Padding(padding:EdgeInsets.only(left: 20,top: 10),child: Text("Spare Request",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),))
                               ],
                             ),
                           ),

                         ),
                       ),
                     ),
                   ],
                 )




               ],
             ),
           )
         ],
       ),

     ),
   );
  }


}