import 'package:ahc/done.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart' as svg;

class SpareRequest extends StatefulWidget{
  SpareRequestState createState()=>SpareRequestState();
}
class SpareRequestState extends State<SpareRequest>{

  var SelectedValueA;
  var SelectedValueB;
  var SelectedValueC;

  var Count=0;


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(75.0),
        child: AppBar(
          centerTitle: true,
          title: Column(
            children: [
              SizedBox(height: 20,),
              Text("Spare Request",style: TextStyle(color: Colors.white),)
            ],
          ),
        ),

      ),
      body: Padding(
        padding: EdgeInsets.only(left: 20,right: 20,top: 10),
        child: ListView(
          children: [
            Column(
              children: [
                SizedBox(height: 20,),
                Container(
                  height: 46,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(7)),
                      border: Border.all(color: Colors.black12)
                  ),
                  child: Row(
                    children: [
                      Expanded(child: Container(child: svg.SvgPicture.asset("assets/menu/starkey.svg"),),flex: 1,),
                      Expanded(child: Container(child:DropdownButtonFormField(
                        icon: Icon(Icons.keyboard_arrow_down_outlined,color: Colors.blue,size: 30,),
                        decoration: InputDecoration.collapsed(hintText: "Star key"),
                        onChanged: (v){
                          setState(() {
                            SelectedValueA=v;

                          });
                        },
                        value: SelectedValueA,
                        items: ["One","Two","Three"].map((v){
                          return DropdownMenuItem(child: Text(v),value: v,);
                        }).toList(),
                      ),),flex: 5,)
                    ],
                  ),
                ),
                SizedBox(height: 20,),
                Container(
                  height: 46,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(7)),
                      border: Border.all(color: Colors.black12)
                  ),
                  child: Row(
                    children: [
                      Expanded(child: Container(child: svg.SvgPicture.asset("assets/menu/battery.svg"),),flex: 1,),
                      Expanded(child: Container(child:DropdownButtonFormField(
                        icon: Icon(Icons.keyboard_arrow_down_outlined,color: Colors.blue,size: 30,),
                        decoration: InputDecoration.collapsed(hintText: "Battery"),
                        onChanged: (v){
                          setState(() {
                            SelectedValueB=v;

                          });
                        },
                        value: SelectedValueB,
                        items: ["One","Two","Three"].map((v){
                          return DropdownMenuItem(child: Text(v),value: v,);
                        }).toList(),
                      ),),flex: 5,)
                    ],
                  ),
                ),
                SizedBox(height: 20,),
                Container(
                  height: 46,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(7)),
                      border: Border.all(color: Colors.black12)
                  ),
                  child: Row(
                    children: [
                      Expanded(child: Container(child: svg.SvgPicture.asset("assets/menu/sc.svg"),),flex: 1,),
                      Expanded(child: Container(child:DropdownButtonFormField(
                        icon: Icon(Icons.keyboard_arrow_down_outlined,color: Colors.blue,size: 30,),
                        decoration: InputDecoration.collapsed(hintText: "EZ 17"),
                        onChanged: (v){
                          setState(() {
                            SelectedValueC=v;

                          });
                        },
                        value: SelectedValueC,
                        items: ["One","Two","Three"].map((v){
                          return DropdownMenuItem(child: Text(v),value: v,);
                        }).toList(),
                      ),),flex: 5,)
                    ],
                  ),
                ),

                SizedBox(height: 20,),
                Container(
                  height: 46,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(7)),
                      border: Border.all(color: Colors.black12)
                  ),
                  child: Row(
                    children: [
                      Expanded(child: Container(child: svg.SvgPicture.asset("assets/menu/battery.svg"),),flex: 1,),
                      Expanded(child: Container(child:Text("Quantity"),),flex: 3,),
                      Expanded(child: Container(
                        margin: EdgeInsets.only(left: 10,right: 10),
                        child: Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [

                          GestureDetector(
                            onTap: (){
                             setState(() {
                                print("Minus Called");

                               if(Count>0){
                                 Count-=1;
                               }
                             });
                            },
                            child: Container(
                              height: 20,
                              width: 20,
                              decoration: BoxDecoration(
                                color: Colors.blue,

                                borderRadius: BorderRadius.all(Radius.circular(20))
                              ),
                              child: Center(child: Text("-",style: TextStyle(color: Colors.white),)),
                            ),
                          ),
                          Padding(padding:EdgeInsets.only(left: 12,right: 12),child: Text(Count.toString())),
                          GestureDetector(
                            onTap: (){
                              setState(() {
                                Count+=1;
                              });
                            },
                            child: Container(
                              height: 20,
                              width: 20,
                              decoration: BoxDecoration(
                                  color: Colors.blue,

                                  borderRadius: BorderRadius.all(Radius.circular(20))
                              ),
                              child: Center(child: Text("+",style: TextStyle(color: Colors.white),)),
                            ),
                          )
                        ],
                      ),),flex: 2,)
                    ],
                  ),
                ),
                SizedBox(height: 20,),
                Container(
                  padding: EdgeInsets.all(10),
                  height: 150,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    border: Border.all(color: Colors.black12)
                  ),
                  child:ListView(
                    
                    children: [
                    Center(
                      child: Column(
                        children: [

                          Row(
                            children: [
                              Expanded(child: Text("Unit Price",style: TextStyle(fontSize: 16),),flex: 1,),
                              Expanded(child: Text("40",textAlign: TextAlign.end,style: TextStyle(fontSize: 16)),flex: 1,)
                            ],
                          ),
                          SizedBox(height: 5,),
                          Row(
                            children: [
                              Expanded(child: Text("Discount",style: TextStyle(fontSize: 16),),flex: 1,),
                              Expanded(child: Text("0",textAlign: TextAlign.end,style: TextStyle(fontSize: 16)),flex: 1,)
                            ],
                          ),
                          SizedBox(height: 5,),
                          Row(
                            children: [
                              Expanded(child: Text("Postal charges",style: TextStyle(fontSize: 16),),flex: 1,),
                              Expanded(child: Text("0",textAlign: TextAlign.end,style: TextStyle(fontSize: 16)),flex: 1,)
                            ],
                          ),
                          SizedBox(height: 27,),
                          Row(
                            children: [
                              Expanded(child: Text("Postal charges",style: TextStyle(fontSize: 19,fontWeight: FontWeight.bold),),flex: 1,),
                              Expanded(child: Text("\$800",textAlign: TextAlign.end,style: TextStyle(fontSize: 19,fontWeight: FontWeight.bold,color: Colors.blue)),flex: 1,)
                            ],
                          ),

                        ],
                      ),
                    )
                  ],),
                ),
                SizedBox(height: 25,),
                Container(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: (){
Navigator.of(context).push(MaterialPageRoute(builder: (context)=>Done("Spare Request Confirm!")));
                        },
                        child: Container(
                          margin: EdgeInsets.only(left: 10,right: 5,),
                          height: 44,
                          width: 120,
                          decoration: BoxDecoration(color: Colors.blue,borderRadius: BorderRadius.all(Radius.circular(8))),
                          child: Center(child: Text("Submit",style: TextStyle(color: Colors.white),)),
                        ),
                      ),
                      GestureDetector(
                        onTap: (){
Navigator.of(context).pop();
                        },
                        child: Container(
                          margin: EdgeInsets.only(left: 10,right: 5,),
                          height: 44,
                          width: 120,
                          decoration: BoxDecoration(color: Colors.blue,borderRadius: BorderRadius.all(Radius.circular(8))),
                          child: Center(child: Text("Cancel",style: TextStyle(color: Colors.white),)),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

}