import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart' as svg;
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart' as SH;
import 'package:toast/toast.dart' as toast;

import 'AppointmentCondirm.dart';
import 'done.dart';
class Appointment extends StatefulWidget{
  AppointmentState createState()=>AppointmentState();
}
class AppointmentState extends State<Appointment>{

  var Typeslist=["One","Two","Three"];
  String SelectedType;

  GlobalKey<FormState> _key=GlobalKey<FormState>();



  var _datecontroller=TextEditingController();
  var _remarks=TextEditingController();
  var _starttimecontroller=TextEditingController();
  var _endtimecontroller=TextEditingController();
  void callDatePicker() async {
    var order = await getDate();
    setState(() {
      _datecontroller.text = order.day.toString()+"-"+order.month.toString()+"-"+order.year.toString();
    });
  }






  void showInSnackBar(String value) {
    Scaffold.of(context).showSnackBar(new SnackBar(
        content: new Text(value)
    ));
  }





  SubmitData()async{
    SH.SharedPreferences sharedPreferences=await SH.SharedPreferences.getInstance();
    var SessionID=sharedPreferences.getString("session_id");
    print("Session = "+SessionID.toString());
    // _key.currentState.save();
    // showInSnackBar("All Form Required");
    // var request=http.MultipartRequest("POST",Uri.parse("http://164.52.205.22/api/newAppointment.php"));
    //     request.fields.addAll({
    //           "session_id":SessionID,
    //           "appointment_date":_datecontroller.text.toString(),
    //           "audiologist_id":"None",
    //           "appointment_fromtime":_starttimecontroller.text.toString(),
    //           "appointment_totime":_endtimecontroller.text.toString(),
    //           "appointment_type":SelectedType,
    //           "appointment_remarks":_remarks.text.toString(),
    //
    //     });
    //
    //     http.StreamedResponse response=await request.send();
    //
    //     if(response.statusCode==200){
    //       var Result=await response.stream.bytesToString();
    //       var ResultJson=json.decode(Result);
    //       if(ResultJson["error"]==false){
    //         toast.Toast.show("Appointment Success.", context);
    //         _datecontroller.clear();
    //         _remarks.clear();
    //         _starttimecontroller.clear();
    //         _endtimecontroller.clear();
    //         setState(() {
    //           SelectedType=null;
    //
    //         });
    //         print(ResultJson);
    //
    //       }else{
    //         toast.Toast.show("Appointment Faild!.", context);
    //       }
    //     }else{
    //       print("Connecton Faild");
    //     }
    if(_key.currentState.validate()){
      Scaffold.of(context).showSnackBar(SnackBar(content: Text("Form was Required")));
    }


  }





  Future ShowStartTime()async{
    showTimePicker(context: context, initialTime: TimeOfDay.now()).then((value){
      var stamp="PM";
   setState(() {
if(value.period.index==0){
  stamp="AM";
}
_starttimecontroller.text=value.hour.toString()+":"+value.minute.toString()+" "+stamp.toString();
   });


    } );

  }
  Future ShowEndTime()async{
    showTimePicker(context: context, initialTime: TimeOfDay.now()).then((value){
      var stamp="PM";
      setState(() {
        if(value.period.index==0){
          stamp="AM";
        }
        _endtimecontroller.text=value.hour.toString()+":"+value.minute.toString()+" "+stamp.toString();
      });


    } );

  }
  
  Future<DateTime> getDate() {
    // Imagine that this function is
    // more complex and slow.
    return showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime.now(),
      lastDate: DateTime(2050),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light(),
          child: child,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {

   return Scaffold(
     appBar: PreferredSize(
       preferredSize: Size.fromHeight(75.0),
       child: AppBar(
         centerTitle: true,
         automaticallyImplyLeading: false,
         title: Column(
           children: [
             SizedBox(height: 20,),
             Text("Book an appointment",style: TextStyle(fontSize: 17),)
           ],
         ),
       ),
     ),
     body: Padding(
       padding: EdgeInsets.only(left: 30,right: 30,top: 25,bottom: 25),
       child: ListView(
         children: [
           Column(
           children: [

  Form(
    key: _key,
    child: Column(
      children: [
        Container(
        height: 50,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(7)),
            border: Border.all(color: Colors.black12)
        ),
        child: Row(
          children: [
            Expanded(child: Container(
              child: svg.SvgPicture.asset("assets/menu/date.svg"),
            ),flex: 1,),
            Expanded(child: Container(
              child: Padding(
                padding: EdgeInsets.only(left: 3),
                child: GestureDetector(
                  onTap: (){
                    callDatePicker();
                  },
                  child: TextFormField(
                    validator: (v){
                      if(v.isEmpty){
                        return "This field is required";
                      }else{
                        return null;
                      }
                    },
                    controller: _datecontroller,
                    autofocus: true,
                    enabled: false,
                    style: TextStyle(color: Colors.black54,fontSize: 12),
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "Select Date",
                        hintStyle: TextStyle(color: Colors.black54,fontSize: 12)
                    ),
                  ),
                ),
              ),
            ),flex: 4,)
          ],
        ),
      ),
        Container(
          margin: EdgeInsets.only(top: 20),
          height: 50,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(7)),
              border: Border.all(color: Colors.black12)
          ),
          child: Row(
            children: [
              Expanded(child: Container(
                child: svg.SvgPicture.asset("assets/menu/search.svg"),
              ),flex: 1,),
              Expanded(child: Container(
                child: Padding(
                  padding: EdgeInsets.only(left: 3),
                  child: TextFormField(

                    controller: _remarks,
                    autofocus: true,
                    style: TextStyle(color: Colors.black54,fontSize: 12),
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "Audiologist",
                        hintStyle: TextStyle(color: Colors.black54,fontSize: 12)
                    ),
                  ),
                ),
              ),flex: 4,)
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 20),
          height: 50,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(7)),
              border: Border.all(color: Colors.black12)
          ),
          child: Row(
            children: [
              Expanded(child: Container(
                child: svg.SvgPicture.asset("assets/menu/time.svg"),
              ),flex: 1,),
              Expanded(child: GestureDetector(
                onTap: (){
                  ShowStartTime();
                },
                child: Container(
                  child: Padding(
                    padding: EdgeInsets.only(left: 3),
                    child: TextFormField(
                      validator: (v){
                        if(v.isEmpty){
                          return "This field is required";
                        }else{
                          return null;
                        }
                      },
                      controller: _starttimecontroller,
                      autofocus: true,
                      enabled: false,
                      style: TextStyle(color: Colors.black54,fontSize: 12),
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "From Time",
                          hintStyle: TextStyle(color: Colors.black54,fontSize: 12)
                      ),
                    ),
                  ),
                ),
              ),flex: 4,)
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 20),
          height: 50,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(7)),
              border: Border.all(color: Colors.black12)
          ),
          child: Row(
            children: [
              Expanded(child: Container(
                child: svg.SvgPicture.asset("assets/menu/time.svg"),
              ),flex: 1,),
              Expanded(child: GestureDetector(
                onTap: (){
                  ShowEndTime();
                },
                child: Container(
                  child: Padding(
                    padding: EdgeInsets.only(left: 3),
                    child: TextFormField(
             controller: _endtimecontroller,
                      autofocus: true,
                      enabled: false,
                      style: TextStyle(color: Colors.black54,fontSize: 12),
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "To Time",
                          hintStyle: TextStyle(color: Colors.black54,fontSize: 12)
                      ),
                    ),
                  ),
                ),
              ),flex: 4,)
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 20),
          height: 50,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(7)),
              border: Border.all(color: Colors.black12)
          ),
          child: Row(
            children: [
              Expanded(child: Container(
                child: svg.SvgPicture.asset("assets/menu/filter.svg"),
              ),flex: 1,),
              Expanded(child: Container(
                child: Padding(
                    padding: EdgeInsets.only(left: 3),
                    child: DropdownButtonFormField(
                      iconEnabledColor: Colors.blue,
                      iconSize: 35,
                      icon: Icon(Icons.keyboard_arrow_down_outlined),
                      decoration: InputDecoration.collapsed(hintText: "Type",hintStyle: TextStyle(fontSize: 12)),
                      value: SelectedType,
                      onChanged: (v){
                        setState(() {
                          SelectedType=v;
                        });
                      },
                      items: Typeslist.map((e) => DropdownMenuItem(child: Text(e,style: TextStyle(fontSize: 12),),value: e,)).toList(),)
                ),
              ),flex: 4,)
            ],
          ),
        ),],
    ),
  ),

             Builder(
               builder:  (context)=>Container(
                 margin: EdgeInsets.only(top: 20),
                 child: Row(
                   crossAxisAlignment: CrossAxisAlignment.center,
                   mainAxisAlignment: MainAxisAlignment.center,
                   children: [

                     GestureDetector(
                       onTap:(){

                         if(_datecontroller.text==""){
                           Scaffold.of(context).showSnackBar(SnackBar(content: Text("Form was Required")));
                         }
                         if(_remarks.text==""){
                           Scaffold.of(context).showSnackBar(SnackBar(content: Text("Form was Required")));
                         }
                         if(_starttimecontroller.text==""){
                           Scaffold.of(context).showSnackBar(SnackBar(content: Text("Form was Required")));
                         }
                         if(_endtimecontroller.text==""){
                           Scaffold.of(context).showSnackBar(SnackBar(content: Text("Form was Required")));
                         }
                         if(SelectedType==null){
                           Scaffold.of(context).showSnackBar(SnackBar(content: Text("Form was Required")));
                         }

                         else{
                           Navigator.of(context).push(MaterialPageRoute(builder: (context)=>AppointmentConfirm("Appointment Confirmed")));

                         }

                       },
                       child: Container(
                         margin: EdgeInsets.only(right: 5),
                         height: 45,
                         width: 130,
                         decoration: BoxDecoration(
                             color: Colors.blue,
                             borderRadius: BorderRadius.all(Radius.circular(7))
                         ),
                         child: Center(child: Text("Submit",style: TextStyle(color: Colors.white,fontSize: 18),)),
                       ),
                     ),
                     GestureDetector(
                       onTap:(){
                         Navigator.of(context).pop();
                       },
                       child: Container(
                         margin: EdgeInsets.only(left: 5),
                         height: 45,
                         width: 130,
                         decoration: BoxDecoration(
                             color: Colors.blue,
                             borderRadius: BorderRadius.all(Radius.circular(7))
                         ),
                         child: Center(child: Text("Cancel",style: TextStyle(color: Colors.white,fontSize: 18),)),
                       ),
                     )

                   ],
                 ),
               ),
             )


           ],
         ),],
       )
     ),
   );
  }

}