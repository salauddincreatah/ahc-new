import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart' as svg;

class Payment extends StatefulWidget{
  PaymentState createState()=>PaymentState();
}
class PaymentState extends State<Payment>{

var  SelectedIndex=1;

  @override
  Widget build(BuildContext context) {


    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(75.0),
        child: AppBar(
          centerTitle: true,
          title:Column(
            children: [
              SizedBox(height: 15,),
              Text("Payment")
            ],
          ),
          backgroundColor: Colors.blue,
        ),
      ),
      body:       Stack(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 40,),
              Center(child: Text("Total amount to pay",style: TextStyle(fontSize: 20),)),
              SizedBox(height: 5,),
              Center(child: Text("\$4,00",style: TextStyle(fontSize: 18),)),

              Row(
                children: [
                  Expanded(child: GestureDetector(
                    onTap: (){
                      setState(() {
                        SelectedIndex=1;
                      });
                    },
                    child: Container(
                      height: 150,
                      padding: EdgeInsets.all(10),
                      child: Card(

                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(7)),
                            side: BorderSide(color: Colors.green,width: 0.3)

                        ),
                        child: Stack(
                          children: [
                            Positioned(top: 0,right: 0,child: Container(child:SelectedIndex==1? svg.SvgPicture.asset("assets/menu/success.svg",height: 13,width: 13,):SizedBox())),
                            Center(child: svg.SvgPicture.asset("assets/menu/card.svg")),
                            Positioned(right: 20,bottom: 5,child:  Text("Card / Debit Card",style: TextStyle(fontSize: 17),textAlign: TextAlign.center,))

                          ],
                        ),),
                    ),
                  )),
                  Expanded(child: GestureDetector(
                    onTap: (){
                      setState(() {
                        SelectedIndex=2;
                      });
                    },
                    child: Container(
                      height: 150,
                      padding: EdgeInsets.all(10),
                      child: Card(

                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(7)),
                            side: BorderSide(color: Colors.green,width: 0.3)

                        ),
                        child: Stack(
                          children: [
                            Positioned(top: 0,right: 0,child: Container(child:SelectedIndex==2? svg.SvgPicture.asset("assets/menu/success.svg",height: 13,width: 13,):SizedBox())),
                            Center(child: svg.SvgPicture.asset("assets/menu/netbank.svg")),
                            Positioned(right: 55,bottom: 5,child:  Text("Net Bank",style: TextStyle(fontSize: 17),textAlign: TextAlign.center,))

                          ],
                        ),),
                    ),
                  )),
                ],
              ),
              Row(
                children: [
                  Expanded(child: GestureDetector(
                    onTap: (){
                      setState(() {
                        SelectedIndex=3;
                      });
                    },
                    child: Container(
                      height: 150,
                      padding: EdgeInsets.all(10),
                      child: Card(

                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(7)),
                            side: BorderSide(color: Colors.green,width: 0.3)

                        ),
                        child: Stack(
                          children: [
                            Positioned(top: 0,right: 0,child: Container(child:SelectedIndex==3? svg.SvgPicture.asset("assets/menu/success.svg",height: 13,width: 13,):SizedBox())),
                            Center(child: svg.SvgPicture.asset("assets/menu/cod.svg")),
                            Positioned(right: 30,bottom: 5,child:  Text("Cash on Delivery",style: TextStyle(fontSize: 17),textAlign: TextAlign.center,))

                          ],
                        ),),
                    ),
                  )),
                  Expanded(child: GestureDetector(
                    onTap: (){
                      setState(() {
                        SelectedIndex=4;
                      });
                    },
                    child: Container(
                      height: 150,
                      padding: EdgeInsets.all(10),
                      child: Card(

                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(7)),
                            side: BorderSide(color: Colors.green,width: 0.3)

                        ),
                        child: Stack(
                          children: [
                            Positioned(top: 0,right: 0,child: Container(child:SelectedIndex==4? svg.SvgPicture.asset("assets/menu/success.svg",height: 13,width: 13,):SizedBox())),
                            Center(child: svg.SvgPicture.asset("assets/menu/gpay.svg")),
                            Positioned(right: 70,bottom: 5,child:  Text("Upi",style: TextStyle(fontSize: 17),textAlign: TextAlign.center,))

                          ],
                        ),),
                    ),
                  )),
                ],
              )
            ],
          ),

          Positioned(
              bottom: 10,
              width: MediaQuery.of(context).size.width,
              child: Container(

                  margin: EdgeInsets.only(left: 20,right: 20),
                  child:Container(
                    decoration: BoxDecoration(
                        color: Colors.lightBlue,
                      borderRadius: BorderRadius.all(Radius.circular(6))
                    ),

                    height: 50,
                    child: Center(child: Text("Process to Pay",style: TextStyle(color: Colors.white,fontSize: 18),)),
                  ) ))
        ],

      )
    );
  }

}